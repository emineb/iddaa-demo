type MatchItemType = {
    bets: BetType[];
    id: number;
    leagueId: number;
    match: string;
}

type LeagueType = {
    id: number;
    title: string;
}

type BetType = {
    id: number;
    type: string;
    desc: string;
    mbs: number;
    possibilities: PossibilityType[];
}

type PossibilityType = {
    result: string;
    rate: number
}

type CouponItem = {
    match: string;
    desc: string;
    result: string;
    rate: number;
    mbs: number;
}
