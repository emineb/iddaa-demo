import React from 'react';
import { Bulten, Coupon, Header, SubMenu } from '@components';
import '@pages/home/home.scss';


export default function Home() {


  return (
    <div>
      <Header />
      <SubMenu />
      <div className='content'>
        <Bulten />
        <Coupon />
      </div>
    </div>
  );
}
