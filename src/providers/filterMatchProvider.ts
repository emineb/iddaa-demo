import {
    debounceTime,
    distinctUntilChanged,
    map,
    switchMap,
    tap
  } from 'rxjs/operators';
  import { fromEvent, of } from 'rxjs';
  