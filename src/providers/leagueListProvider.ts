import { leagueSubject } from 'subject/leagueSubject';
import { leaguesData } from 'services';


export function getLeagueList() {

    leaguesData.subscribe({
        error: (error) => console.log(error),
        next: (data) => {
            leagueSubject.next({
                ...leagueSubject.value,
                leagueList: data as LeagueType[]
            });
        },
    });

}