import { eventsData } from 'services';
import { matchSubject } from 'subject/matchSubject';

export function getMatchList() {

    eventsData.subscribe({
        error: (error) => console.log(error),
        next: (data) => {
            matchSubject.next({
                ...matchSubject.value,
                matchList: data as MatchItemType[]
            });
        },
    });



}