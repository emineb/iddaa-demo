
const EVENTS_API = 'http://localhost:4000/events';
const LEAGUES_API = 'http://localhost:4000/leagues';

import { fromFetch } from 'rxjs/fetch';
import { of } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';

export const eventsData = fromFetch(EVENTS_API).pipe(
    switchMap(response => {
        if (response.ok) {

            return response.json();
        } else {
            return of({ error: true, message: `Error ${response.status}` });
        }
    }),
    catchError(err => {
        console.error(err);

        return of({ error: true, message: err.message });
    })
);

export const leaguesData = fromFetch(LEAGUES_API).pipe(
    switchMap(response => {
        if (response.ok) {

            return response.json();
        } else {

            return of({ error: true, message: `Error ${response.status}` });
        }
    }),
    catchError(err => {
        console.error(err);

        return of({ error: true, message: err.message });
    })
);