import { Badge } from '@components';
import { Plus } from '@components/icons';
import { addCoupon } from 'handler/couponHandler';
import { getLeagueList } from 'providers/leagueListProvider';
import { getMatchList } from 'providers/matchListProvider';
import { leagueSubject } from 'subject/leagueSubject';
import { matchSubject } from 'subject/matchSubject';
import { searchSubject } from 'subject/searchSubject';
import React, { Component } from 'react';
import '@components/bulten/bulten.scss';


class Bulten extends Component {

    state = { filteredMatchData: new Array<MatchItemType>(), leagueData: new Array<LeagueType>(), matchData: new Array<MatchItemType>() }

    componentDidMount() {
        getLeagueList();
        getMatchList();

        searchSubject.subscribe((input) => {

            const input2String = input as string;
            const filteredMatches = (input2String === '') ? this.state.matchData : this.state.matchData.filter(h => h.match.toLowerCase().includes(input2String));

            this.setState({ filteredMatchData: filteredMatches });
        });

        matchSubject.subscribe((response) => {
            const matchList = response.matchList;
            this.setState({ matchData: matchList });
            this.setState({ filteredMatchData: matchList });
        });

        leagueSubject.subscribe((response) => {
            this.setState({ leagueData: response.leagueList });
        });
    }

    renderBadge(mbs: number) {

        switch (mbs) {
            case 1:
                return <Badge red>{mbs}</Badge>;
            case 2:
                return <Badge orange>{mbs}</Badge>;
            default:
                return <Badge green>{mbs}</Badge>;
        }
    }

    league(id: number) {

        const leagueTitle = this.state.leagueData.find(c => c.id == id);

        return leagueTitle.title.slice(0, 3);
    }
    
    render() {
        return (
            <div className='bulten'>
                <div className='bulten-text'>
                    <h1 className='bulten-text-header'>BÜLTEN</h1>
                    <p className='bulten-text-date'>28 Mart Pazar, 21:00</p>
                </div>
                <div>
                    <table className='bulten-table' >
                        <tr className='bulten-table-row'  >
                            <th className='bulten-table-row-border' rowSpan={2}>MBS</th>
                            <th rowSpan={2}>LİG</th>
                            <th rowSpan={2}>EV SAHİBİ-KONUK TAKIM</th>
                            <th colSpan={3}>MAÇ SONUCU</th>
                            <th colSpan={3}>ÇİFTE ŞANS</th>
                            <th colSpan={2}>ALT/ÜST</th>
                            <th rowSpan={2}><Plus /></th>
                        </tr>
                        <tr className='bulten-table-span' >
                            <th>1</th>
                            <th>0</th>
                            <th>2</th>
                            <th>1-X</th>
                            <th>1-2</th>
                            <th>2-X</th>
                            <th>Alt</th>
                            <th>Üst</th>
                        </tr>
                        {

                            this.state.filteredMatchData.map((match) => (
                                <tr id='bulten-table-deneme' key={match.id} className='bulten-table-data'>
                                    <td className='bulten-table-row-border'>{this.renderBadge.bind(this, match.bets[0].mbs)()}</td>
                                    <td>{this.league(match.leagueId)}..</td>
                                    <td>{match.match}</td>
                                    <td id='bulten-table' className='bulten-table-datacell' onClick={() => addCoupon(match, 0, 0)}>{match.bets[0].possibilities[0].rate}</td>
                                    <td id='bulten-table' className='bulten-table-datacell' onClick={() => addCoupon(match, 0, 1)}>{match.bets[0].possibilities[1].rate}</td>
                                    <td id='bulten-table' className='bulten-table-datacell' onClick={() => addCoupon(match, 0, 2)}>{match.bets[0].possibilities[2].rate}</td>
                                    <td id='bulten-table' className='bulten-table-datacell' onClick={() => addCoupon(match, 2, 0)}>{match.bets[2].possibilities[0].rate}</td>
                                    <td id='bulten-table' className='bulten-table-datacell' onClick={() => addCoupon(match, 2, 1)}>{match.bets[2].possibilities[1].rate}</td>
                                    <td id='bulten-table' className='bulten-table-datacell' onClick={() => addCoupon(match, 2, 2)}>{match.bets[2].possibilities[2].rate}</td>
                                    <td id='bulten-table' className='bulten-table-datacell' onClick={() => addCoupon(match, 1, 0)}>{match.bets[1].possibilities[0].rate}</td>
                                    <td id='bulten-table' className='bulten-table-datacell' onClick={() => addCoupon(match, 1, 1)}>{match.bets[1].possibilities[1].rate}</td>
                                    <td id='bulten-table' className='bulten-table-datacell'>+12</td>
                                </tr>
                            ))}
                    </table>
                </div>
            </div>
        );
    }

}

export default Bulten;



