import ClassNames from 'classnames';

import React, { ComponentPropsWithoutRef, ElementType } from 'react';
import '@components/badges/badge.scss';

type IButtonProps<T extends ElementType<unknown>> = ComponentPropsWithoutRef<T> & {

    green?: boolean; //3
    orange?: boolean; //2
    red?: boolean;  //1
    yellow?: boolean;
}

export default function Badge({ children, green, orange, red, yellow, ...props }: IButtonProps<'span'>) {

    return (
        <span
            {...props}
            className={ClassNames('badge', {
                'badge-green': green,
                'badge-orange': orange,
                'badge-red': red,
                'badge-yellow': yellow
            })}
        >
            {children}
        </span>
    );
}