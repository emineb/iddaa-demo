import React, { Component } from 'react';

import { ChevronDown } from '@components/icons';
import { CouponsItem } from '@components';
import { couponSubject } from 'subject/couponSubject';
import { removeCoupon } from 'handler/couponHandler';
import '@components/coupon/coupon.scss';

function emptyCoupon() {
    return (
        <div className='emptyCoupon'>
            <div className='emptyCoupon-myCuopon'>
                <h1>KUPONUM</h1>
                <p className='emptyCoupon-myCuopon-rate' >T. Oran: 0,00 TL</p>
            </div>
            <div className='emptyCoupon-number'><span>0 Maç</span> </div>
        </div>
    );
}

function fullCoupon(couponList, sum) {
    return (
        <div className='full'>
            <div className='fullCoupon'>
                <div className='fullCoupon-myCuopon'>
                    <h1>KUPONUM</h1>
                    <p className='fullCoupon-myCuopon-rate' >T. Oran: {sum} </p>
                </div>
                <div className='fullCoupon-dropdown'>
                    <div className='fullCoupon-number'><span>{couponList.length} Maç</span>
                    </div>
                <ChevronDown/>  
                </div>
            </div>
            <CouponsItem></CouponsItem>
            <div className='fullCoupon-container'>
                <div className='fullCoupon-footer'>
                    <div className='fullCoupon-footer-text'>
                        <h1>KUPON BEDELİ</h1>
                        <h1>TOPLAM ORAN</h1>
                        <h1>MAKSİMUM KAZANÇ</h1>
                    </div>
                    <div className='fullCoupon-footer-price'>
                        <p>3,00 TL</p>
                        <p>{sum}</p>
                        <p className='fullCoupon-footer-gain'> {(sum * 3).toFixed(2)} TL</p>
                    </div>
                </div>
                <div className='fullCoupon-play'>
                    <button className='fullCoupon-play-button' >HEMEN OYNA</button>
                </div>
            </div>
        </div>

    );
}

class Coupon extends Component {
    state = { couponList: new Array<CouponItem>() };

    componentDidMount() {
        couponSubject.subscribe((response) => {
            this.setState({ couponList: response.lists });
        });
    }

    sum(couponItems) {
        let couponSum = 1;
        couponItems.map((coupons) => (
            couponSum = couponSum * coupons.rate
        ));

        return couponSum.toFixed(2);
    }



    render() {
        const couponsList = this.state.couponList;

        return (
            <div className='coupon'>
                {couponsList.length === 0 ? emptyCoupon() :
                    fullCoupon(couponsList, this.sum(couponsList))}</div>
        );
    }
}
export default Coupon;