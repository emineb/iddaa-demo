import React, { Component } from 'react';

import Badge from '@components/badges/Badge';
import { Trash2 } from '@components/icons/index';
import { couponSubject } from 'subject/couponSubject';
import { removeCoupon } from 'handler/couponHandler';
import '@components/coupon/couponItem.scss';

class CouponsItem extends Component {

    state = { couponList: new Array<CouponItem>() };

    componentDidMount() {
        couponSubject.subscribe((response) => {
            this.setState({ couponList: response.lists });
        });
    }
    renderBadge(mbs:number) {


        switch (mbs) {
            case 1:
              return <Badge red>{mbs}</Badge>;
              case 2:
                return <Badge orange>{mbs}</Badge>;
            default:
                 return <Badge green>{mbs}</Badge>;
              
        }

    }

    render() {
        return (
            <div>
                {this.state.couponList.map((match) => (
                    <div className='couponItem'>
                       {this.renderBadge.bind(this,match.mbs)()}<div className='couponItem-text'>
                            <h1 className='couponItem-text-match'>{match.match}</h1>
                            <p className='couponItem-text-date'>28 Mart Pazar, 19:00</p>
                            <div className='couponItem-result'>
                                <p className='couponItem-result-text'>{match.desc}</p>
                                <p className='couponItem-result-final'>{match.result}</p>
                            </div>
                        </div>
                        <div className='couponItem-trash'>
                            <Trash2 onClick={() => removeCoupon(match)}></Trash2>
                            <p className='couponItem-trash-rate'>{match.rate}</p>
                        </div>
                    </div>
                ))}
            </div>

        );
    }

}
export default CouponsItem;
