import React from 'react';
import { Badge, Button, Search } from '@components';
import '@components/submenu/subMenu.scss';

export default function SubMenu() {
    return (
        <div className='coverSub'>
            <div className={'subMenu'}>
                <div className='subMenu-button'>
                    <Button withBadge>BÜLTEN<Badge yellow>09</Badge></Button>
                    <Button withBadge>CANLI<Badge yellow>09</Badge></Button>
                </div>
                <div className='subMenu-search'>
                <Search />
                </div>
            </div>
        </div>

    );
}

