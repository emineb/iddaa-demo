import { Button } from '@components';
import React from 'react';

import { Logo } from '@components/icons/index';
import '@components/header/header.scss';

export default function Header() {
    return (
        <div className='coverHead'>
            <div className={`header`}>
                <Logo className='header-logo' />
                <div className='header-button'>
                    <Button primary >GİRİŞ YAP</Button>
                    <Button secondary >ÜYE OL</Button>
                </div>
            </div>
        </div>
    );
}

