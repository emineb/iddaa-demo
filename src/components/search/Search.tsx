import React from 'react';
import { Searchs } from '@components/icons/index';

import { onInputChange } from 'handler/searchHandler';
import '@components/search/search.scss';

export default function Search({ ...props }) {


    setInterval(function () { onInputChange(); }, 1000);

    return (
        <div className='input-search'>
            <i className='input-search-icon'><Searchs /></i>
            <input id='input-search-field' className='input-search-field' type='search'
                placeholder='Search' {...props} ></input>
        </div>

    );
}

