export { default as Text } from '@components/text/Text';
export { default as Button } from '@components/buttons/Button';
export { default as Header } from '@components/header/Header';
export { default as Search } from '@components/search/Search';
export { default as SubMenu } from '@components/submenu/SubMenu';
export { default as Badge } from '@components/badges/Badge';
export { default as Bulten } from '@components/bulten/Bulten';
export { default as CouponsItem } from '@components/coupon/CouponsItem';
export { default as Coupon } from '@components/coupon/Coupon';