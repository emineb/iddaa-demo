import * as React from 'react';


function SvgChevronDown(props) {
  return (
    <svg
      width='24px'
      height='24px'
      viewBox='0 0 24 24'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
    {...props}
 

>
      <path
        d='M6 9l6 6 6-6'
        stroke='currentColor'
        strokeWidth={2}
        strokeLinecap='round'
        strokeLinejoin='round'
      />
    </svg>
  );
}

const MemoSvgChevronDown = React.memo(SvgChevronDown);
export default MemoSvgChevronDown;
