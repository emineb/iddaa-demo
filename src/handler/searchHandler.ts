import { fromEvent } from 'rxjs';
import { searchSubject } from 'subject/searchSubject';
import {
    debounceTime,
    map,
} from 'rxjs/operators';

export function onInputChange() {
    fromEvent(document.getElementById('input-search-field'), 'keyup')
        .pipe(
            debounceTime(200),
            map((e: any) => searchSubject.next(e.target.value)),
        )
        .subscribe();
}