import { couponSubject } from 'subject/couponSubject';

export const addCoupon = (data: MatchItemType, betIndex: number, possibilityIndex: number) => {


    const bet = {} as CouponItem;
    bet.match = data.match;
    const currentBet = data.bets[betIndex];
    bet.desc = currentBet.desc;
    const currentPossiblity = currentBet.possibilities[possibilityIndex];
    bet.rate = currentPossiblity.rate;
    bet.result = currentPossiblity.result;
    bet.mbs = currentBet.mbs;



    couponSubject.next({
        ...couponSubject.value,
        lists: getNewItems(bet, [...couponSubject.value.lists]),
        
    });

    function getNewItems(newItem: CouponItem, currentList: CouponItem[]) {

        const addedItem = currentList.find(c => c.match === newItem.match);

        if (addedItem) {

            const k = currentList.indexOf(addedItem);
            currentList.splice(k, 1);
        }

        else {
            currentList = [...currentList, newItem];
        }

        return currentList;
    }



};

export const removeCoupon = (data: CouponItem) => {

    couponSubject.next({
        ...couponSubject.value,
        lists: [...couponSubject.value.lists.filter(c => c.match !== data.match)]
    });

};