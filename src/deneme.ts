import {
  debounceTime,
  distinctUntilChanged,
  map,
  switchMap,
  tap
} from 'rxjs/operators';
import { fromEvent, of } from 'rxjs';

const aIleBaslayanSehirler = keys =>
  [
    'ankara',
    'adana',
    'ardahan',
    'adıyaman',
    'afyonkarahisar',
    'artvin',
    'aydın'
  ].filter(e => e.indexOf(keys.toLowerCase()) > -1);

const observable$ = keys =>
  of(aIleBaslayanSehirler(keys)).pipe(tap(searc => console.log(searc)));

fromEvent(document.getElementById('arama'), 'keyup')
  .pipe(
    //   200ms bekleme
    debounceTime(200),
    map((e: any) => e.target.value),
    // aynı değer girilirse göz ardı  edecektir. "a" harfine basılı tutunca peşi sıra "a" harfi yazılmayacak
    distinctUntilChanged(),
    // bir istek sonuçlanmadan yeni bir arama yapılırsa bir önceki istek iptal edilecek
    switchMap(observable$),
    // gelen değer client tarafında işlenip, ekrana yazılıyor
    tap(c => (document.getElementById('cikti').innerText = c.join('\n')))
  )
  .subscribe();
