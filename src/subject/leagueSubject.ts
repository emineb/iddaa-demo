import { BehaviorSubject } from 'rxjs';

export const leagueSubject = new BehaviorSubject({

    leagueList: new Array<LeagueType>(),
});
